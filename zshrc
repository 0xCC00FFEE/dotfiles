#Set up language
export LANG="es_ES.UTF-8"

#Fix color warning on terminal
export TERM="xterm-256color"

#OhMyZSH Stuff
export ZSH="/home/luke/.oh-my-zsh"
#ZSH_THEME="lukerandall"
#ENABLE_CORRECTION="true"

#Powerline 9k theme
#POWERLEVEL9K_MODE='nerdfont-fontconfig'
#POWERLEVEL9K_COLOR_SCHEME='dark'

#POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(os_icon context dir vcs)
#POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=()

#POWERLEVEL9K_VCS_GIT_GITLAB_ICON=$'\uF296  '

#POWERLEVEL9K_VIRTUALENV_BACKGROUND='000'
#POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND='233'
#POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND='white'
#POWERLEVEL9K_DIR_HOME_BACKGROUND='032'
#POWERLEVEL9K_DIR_HOME_FOREGROUND='white'
#POWERLEVEL9K_DIR_HOME_SUBFOLDER_BACKGROUND='024'
#POWERLEVEL9K_DIR_HOME_SUBFOLDER_FOREGROUND='white'
#POWERLEVEL9K_OS_ICON_BACKGROUND='grey0'
#POWERLEVEL9K_OS_ICON_FOREGROUND='deepskyblue3'
#POWERLEVEL9K_VCS_CLEAN_FOREGROUND='green'
#POWERLEVEL9K_VCS_CLEAN_BACKGROUND='grey11'
#POWERLEVEL9K_VCS_UNTRACKED_FOREGROUND='yellow'
#POWERLEVEL9K_VCS_UNTRACKED_BACKGROUND='grey11'
#POWERLEVEL9K_VCS_MODIFIED_FOREGROUND='red'
#POWERLEVEL9K_VCS_MODIFIED_BACKGROUND='grey11'
#POWERLEVEL9K_STATUS_OK_BACKGROUND='springgreen2'
#POWERLEVEL9K_STATUS_OK_FOREGROUND='black'
#POWERLEVEL9K_STATUS_ERROR_BACKGROUND='red'
#POWERLEVEL9K_STATUS_ERROR_FOREGROUND='black'

# Supposed to speed up VCS on large repos
# DISABLE_UNTRACKED_FILES_DIRTY="true"

#Load up plugins
#plugins=(git)
source /home/luke/dev/make/zsh-git-prompt/zshrc.sh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZSH/oh-my-zsh.sh
#source /home/luke/.oh-my-zsh/custom/themes/powerlevel10k/powerlevel10k.zsh-theme

setopt PROMPT_SUBST
PROMPT=' %F{93}%n %F{63}${(%):-%~}%F{blue}  » %F{white}'

#Set up aliases
alias zshc="vim ~/.zshrc"
alias vimc="vim ~/.vimrc"
alias bsc="vim /home/luke/.config/bspwm/bspwmrc"
alias kbc="vim /home/luke/.config/sxhkd/sxhkdrc"
alias polyc="vim /home/luke/.config/polybar/config"
alias rofic="vim ~/.config/rofi/luke.rasi"
alias alac="vim ~/.config/alacritty/alacritty.yml"
alias ma="remarkable &> /dev/null"
alias fuckint="sudo netctl restart wlp6s0-MI6surveillance"
alias fuckswp="rm /home/luke/.cache/vim/swap/*"
alias vi="vim"
alias mmp="sudo openvpn /home/luke/Documents/nl-free-01.protonvpn.com.udp.ovpn"
alias yay="yay --color auto"
alias pacman="pacman --color auto"
alias up="yay -Syu"
alias upd="sudo reflector -l 200 -f 200 -c \"United Kingdom\" --sort rate --save /etc/pacman.d/mirrorlist && yay -Syu"
alias :q="exit"
alias drag="yay -S"
alias qt='notify-send --icon="/home/luke/pic/panda.jpg" "I am a panda" "I like eating a shit load of bamboo and sleeping zzzz"'
alias ca="bat"
alias q="exit"
figlet -t "Welcome, $(whoami | sed 's/./\u&/')" | lolcat --spread 1 --seed 18

