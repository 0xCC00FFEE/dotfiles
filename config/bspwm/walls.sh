hour=$(date +%H)
if (( $hour > 4 && $hour < 19 )); then
    # If it is not evening
    feh --bg-scale /home/luke/pic/wallpaper.jpg
else
    # If it is evening
    feh --bg-scale /home/luke/pic/wallpapernight.png
fi
